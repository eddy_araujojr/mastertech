const botaoIniciar = document.querySelector("#iniciar");
const mesa = document.querySelector("#mesa");
let pontos = document.querySelector("#pontos");
const botaoCarta = document.querySelector("#carta");
const botaoParar = document.querySelector("#parar");
const msg = document.querySelector("#acoes p");

let idBaralho;

function extrairJSON(resposta) {
    return resposta.json();
}

function atualizarPlacar(valor) {
    let placar = Number(pontos.innerHTML) + valor
    if (placar > 20) {
        if (placar === 21) {
            msg.innerHTML = "Cê é zica memo, hein? Fez 21, ganhou!";
        } else if (placar > 21) {
            msg.innerHTML = `Perdeu, playboy... Você estourou com ${placar} pontos.`;
        }
        botaoIniciar.onclick = iniciarJogo;
        botaoCarta.onclick = null;
        botaoParar.onclick = null;
    }
    else {
        pontos.innerHTML = placar;
    }
}

function desenharCarta(carta) {
    let desenho = document.createElement("img");
    desenho.src = carta.image;
    desenho.alt = carta.value + " - " + carta.suit;
    mesa.appendChild(desenho);
    switch (carta.value) {
        case "ACE":
            valor = 1
            break;
        case "QUEEN":
        case "KING":
        case "JACK":
            valor = 10;
            break;
        default:
            valor = Number(carta.value);
            break;
    }
    atualizarPlacar(valor)
}

function preencherMesa(dados) {
    console.log("Preencher mesa." + JSON.stringify(dados));
    let cartas = dados.cards;
    for (const carta of cartas) {
        desenharCarta(carta);
    }
}

function iniciarMesa(dados) {
    console.log("Iniciar mesa. " + JSON.stringify(dados));
    idBaralho = dados.deck_id;
    //fetch(`https://deckofcardsapi.com/api/deck/${idBaralho}/draw/?count=2`)
    fetch("https://deckofcardsapi.com/api/deck/fcw8hc1zm6qa/draw/?count=2")
        .then(extrairJSON)
        .then(preencherMesa);
}

function novaCarta() {
    //fetch(`https://deckofcardsapi.com/api/deck/${idBaralho}/draw/?count=1`)
    fetch("https://deckofcardsapi.com/api/deck/fcw8hc1zm6qa/draw/?count=1")
        .then(extrairJSON)
        .then(preencherMesa);
}

function pararJogo() {
    msg.innerHTML = `Você parou com ${pontos.innerHTML} pontos.`;
    botaoIniciar.onclick = iniciarJogo;
    botaoCarta.onclick = null;
    botaoParar.onclick = null;
}

function iniciarJogo() {
    mesa.innerHTML = "";
    msg.innerHTML = "<p>Pontuação: <span id=\"pontos\">0</span> </p>";
    pontos = document.querySelector("#pontos");
    //fetch("https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1")
    fetch("https://deckofcardsapi.com/api/deck/fcw8hc1zm6qa/shuffle/")
        .then(extrairJSON)
        .then(iniciarMesa);
    botaoIniciar.onclick = null;
    botaoCarta.onclick = novaCarta;
    botaoParar.onclick = pararJogo;
}

botaoIniciar.onclick = iniciarJogo;