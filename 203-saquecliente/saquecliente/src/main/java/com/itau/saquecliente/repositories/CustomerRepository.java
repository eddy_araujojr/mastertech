package com.itau.saquecliente.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.saquecliente.models.Customer;
/**
 * CustomerRepository
 */
public interface CustomerRepository extends CrudRepository<Customer, Long>{
    public Customer findByUsername(String username);
}