package com.itau.saquecliente.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import com.itau.saquecliente.models.Customer;
import com.itau.saquecliente.models.WithdrawRequest;
import com.itau.saquecliente.repositories.CustomerRepository;
import com.itau.saquecliente.services.RatesConverter;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * CustomerController
 */
@RestController
public class CustomerController {

	@Autowired
	CustomerRepository customerRespository;
	RatesConverter ratesConverter;

	@RequestMapping(method=RequestMethod.POST, path="/saque")
	public Customer withdraw(@Valid @RequestBody WithdrawRequest requisicao){
		ratesConverter = new RatesConverter();
		
		double valor = ratesConverter.convert(requisicao.getCurrency(), requisicao.getAmount());
		
		Customer cliente = customerRespository.findById(requisicao.getClientId()).get();
		
		double saldo = Double.parseDouble(cliente.getBalance());
		saldo = saldo - valor;
		cliente.setBalance(Double.toString(saldo));
		
		customerRespository.save(cliente);
		
		return cliente;
	}

	@RequestMapping(path="/clientes")
	public Iterable<Customer> buscarClientes(){
		return customerRespository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/clientes")
	public Customer cadastrarClientes(@Valid @RequestBody Customer cliente) {
		return customerRespository.save(cliente);
	}
}