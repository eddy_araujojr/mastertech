package com.itau.saquecliente.models;

import java.util.HashMap;

/**
 * RatesResponse
 */
public class RatesResponse {

    private String base;
    private long timestamp;
    private String date;
    //private HashMap rates;

    /**
     * @return the base
     */
    public String getBase() {
        return base;
    }
    /**
     * @param base the base to set
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * @return the timestamp
     */
    public long getTimestamp() {
        return timestamp;
    }
    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the date
     */
    public String getDate() {
        return date;
    }
    /**
     * @param date the date to set
     */
    public void setDate(String date) {
        this.date = date;
    }
    
    /**
     * @return the rates
     */
//    public HashMap getRates() {
//        return rates;
//    }
//    /**
//     * @param rates the rates to set
//     */
//    public void setRates(HashMap rates) {
//        this.rates = rates;
//    }
}