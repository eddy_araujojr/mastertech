package jogo_21;

public class Carta {
	private int id;
	public int valor;
	public String figura;
	public Naipe naipe;
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return figura + naipe;
	}
	
	public Carta(int id, Naipe naipe) {
		this.naipe = naipe;
		switch (id) {
		case 1:
			valor = 11;
			figura = "A";
			break;
		case 11:
			valor = 10;
			figura = "J";
			break;
		case 12:
			valor = 10;
			figura = "Q";
			break;
		case 13:
			valor = 10;
			figura = "K";
			break;
		default:
			valor = id;
			figura = String.valueOf(id);
			break;
		}
	}
}
