package jogo_21;

import java.util.Scanner;

public class Jogo {
	public static void main(String[] args) {
		Mesa mesa = new Mesa();
		
		System.out.println("Bem vindo ao jogo!");
		
		Mao mao = new Mao();
		
		mesa.sacarCartas(mao, 1);
		
		String opcao = "S";
		Scanner scan = new Scanner(System.in);
		
		while (opcao.equals("S")) {
			mesa.sacarCartas(mao, 1);

			System.out.println("Suas cartas são: ");
			for (Carta carta : mao.cartas) {
				System.out.print(carta + " | ");
			}
			System.out.println("");
			System.out.println("Você tem " + mao.pontos() + " pontos");
			if(mao.pontos() < 21) {
				System.out.println("Você deseja sacar mais uma carta? S/N");
				opcao = scan.nextLine().toUpperCase();
			}
			else 
				opcao = "N";
			
		}
		
		if (mao.pontos() < 21) {
			System.out.println("Voce parou com " + mao.pontos());
		} else if (mao.pontos() == 21) {
			System.out.println("Ai sim hein");
		} else {
			System.out.println("Você estourou com " + mao.pontos());
		}
		
	}
}
