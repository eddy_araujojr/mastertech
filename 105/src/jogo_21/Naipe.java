package jogo_21;

public enum Naipe {
	ESPADA(0),
	COPAS(1),
	PAUS(2),
	OURO(3);
	
	private int valor;
	
	private Naipe(int valor) {
		this.valor = valor;
	}
	
	public static Naipe naipePorCodigo(int valor) {
		for (Naipe naipe : Naipe.values()) {
			if(naipe.valor == valor)
				return naipe;
		}
		return null;
	}
}
