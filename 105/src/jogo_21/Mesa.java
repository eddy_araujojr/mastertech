package jogo_21;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class Mesa {
	public List<Carta> baralho;
	public Mesa() {
		
		baralho = new ArrayList<Carta>();
		for (int i = 0; i < 4; i++) {
			for (int j = 1; j <= 13; j++) {
				baralho.add(new Carta(j, Naipe.naipePorCodigo(i)));
			}
		}
		Embaralhar();
	}
	
	public void sacarCartas(Mao mao, int numCartas) {
		for (int i = 0; i < numCartas; i++) {
			mao.cartas.add(baralho.remove(0));
		}
	}
	
	public static int sorteando(HashSet<Integer> sorteados, int max) {
		int random;
		do {
			 random = (int)(Math.ceil(Math.random() * max));
		} while (sorteados.contains(random));
		sorteados.add(random);
		return random;
	}
	
	public void Embaralhar() {
		int random;
		List<Carta> baralho2 = new ArrayList<Carta>();
		do {
			 random = (int)(Math.floor(Math.random() * baralho.size()));
			 Carta carta = baralho.remove(random);
			 baralho2.add(carta);
		} while (baralho.size() > 0);
		baralho = baralho2;
	}

}
