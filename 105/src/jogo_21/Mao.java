package jogo_21;

import java.util.ArrayList;
import java.util.List;

public class Mao {
	public List<Carta> cartas;
	
	public Mao() {
		cartas = new ArrayList<Carta>();
	}
	
	public int pontos() {
		int pontos = 0;
		List<Carta> asQueValem11 = new ArrayList<Carta>();
		
		for (Carta carta : cartas) {
			pontos = pontos + carta.valor;
			if(carta.figura == "A" && carta.valor == 11)
				asQueValem11.add(carta);
		}
		if (asQueValem11.size() > 0) {
			while (pontos > 21 && asQueValem11.size() > 0) {
				asQueValem11.get(0).valor = 1;
				asQueValem11.remove(0);
				pontos = pontos - 10;
			}
		}
		return pontos;
	}
}
