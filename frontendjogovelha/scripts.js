const apiUrl = 'http://196.99.103.132/api/';
let header = document.querySelector('header');
let section = document.querySelector('section');
let button = document.querySelector('button');

let map = [
    {x: 0, y: 0},
    {x: 0, y: 1},
    {x: 0, y: 2},
    {x: 1, y: 0},
    {x: 1, y: 1},
    {x: 1, y: 2},
    {x: 2, y: 0},
    {x: 2, y: 1},
    {x: 2, y: 2}
];

function populateDivs(){
    for(let i = 0; i < 9; i++){
        let div = document.createElement('div');
        
        div.onclick = function(){
            let body = getDivMap(this); 
    
            post('jogar', body).then(parseData);
        }

        div.dataBody = map[i]; 

        div.onclick = function(){   
            post('jogar', this.dataBody).then(parseData);
        }

        section.appendChild(div);
    }
}

function get(endpoint){
    return fetch(apiUrl + endpoint, {
        method: 'get',
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

function post(endpoint, data){
    return fetch(apiUrl + endpoint,
    {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'content-type': 'application/json'
        }
    }).then((response) => {
        return response.json();
    });
}

function getDivMap(div){
    let i = 0;

    for(let child of section.children){
        if(child === div){
            return map[i];
        }
    }
}

function parseData(placar){
    let divs = document.querySelectorAll('section div');
    let i = 0;

    for(let linha of placar.casas){
        for(let casa of linha){
            divs[i].innerHTML = casa;
            i++;
        }
    }

    header.innerHTML = `${placar.pontuacao[0]} x ${placar.pontuacao[1]}`;

    button.style.display = placar.encerrado ? 'block' : 'none';
}

button.onclick = () => {
    post('iniciar').then(parseData);
}

populateDivs();
get('jogo').then(parseData);