package com.itau.escola.repositories;

import com.itau.escola.models.Endereco;

import org.springframework.data.repository.CrudRepository;

/**
 * EnderecoRepository
 */
public interface EnderecoRepository extends CrudRepository<Endereco, Long> {

}