package com.itau.escola.repositories;

import com.itau.escola.models.Materia;

import org.springframework.data.repository.CrudRepository;

/**
 * MateriaRepository
 */
public interface MateriaRepository extends CrudRepository<Materia, Long>{

}