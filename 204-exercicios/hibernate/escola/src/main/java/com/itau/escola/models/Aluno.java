package com.itau.escola.models;

import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Aluno
 */
@Entity
public class Aluno extends Pessoa {

    @NotNull
    @Size(min=8, max=8)
    private String ra;

    /**
     * @return the ra
     */
    public String getRa() {
        return ra;
    }
    /**
     * @param ra the ra to set
     */
    public void setRa(String ra) {
        this.ra = ra;
    }

}