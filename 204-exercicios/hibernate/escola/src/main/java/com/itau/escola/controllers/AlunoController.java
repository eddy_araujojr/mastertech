package com.itau.escola.controllers;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.escola.models.Aluno;
import com.itau.escola.repositories.AlunoRepository;

@RestController
public class AlunoController {
	
	@Autowired
	AlunoRepository alunoRepository;
	
	@RequestMapping(method=RequestMethod.GET, path="/alunos")
	public Iterable<Aluno> buscarAlunos(){
		return alunoRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/alunos/{id}")
	public ResponseEntity<?> buscarAlunos(@PathVariable long id){
		Optional<Aluno> aluno = alunoRepository.findById(id);
		if(!aluno.isPresent())
			return ResponseEntity.notFound().build();
		return ResponseEntity.ok().body(aluno.get());
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/alunos")
	public ResponseEntity<?> cadastrarAluno(@Valid @RequestBody Aluno aluno){
		try {
			Aluno novoaluno = alunoRepository.save(aluno);
			return ResponseEntity.ok().body(aluno);
		}
		catch (Exception ex) {
			throw ex;
		}
	}

	@RequestMapping(method=RequestMethod.DELETE, path="/alunos/{id}")
	public ResponseEntity<?> deletarAluno(@PathVariable long id) {
		Optional<Aluno> aluno = alunoRepository.findById(id);
		if(!aluno.isPresent())
			return ResponseEntity.notFound().build();
		try {
			alunoRepository.deleteById(id);
			return ResponseEntity.ok().build();
		}
		catch (Exception ex) {
			throw ex;
		}
	}
}
