package com.itau.escola.repositories;

import com.itau.escola.models.Aluno;

import org.springframework.data.repository.CrudRepository;

/**
 * AlunoRepository
 */
public interface AlunoRepository extends CrudRepository<Aluno, Long> {
}