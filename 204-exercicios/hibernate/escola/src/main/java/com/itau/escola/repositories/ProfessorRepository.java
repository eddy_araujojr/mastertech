package com.itau.escola.repositories;

import com.itau.escola.models.Professor;

import org.springframework.data.repository.CrudRepository;

/**
 * ProfessorRepository
 */
public interface ProfessorRepository extends CrudRepository<Professor, Long> {

    
}