package com.itau.escola.repositories;

import com.itau.escola.models.Turma;

import org.springframework.data.repository.CrudRepository;

/**
 * TurmaRepository
 */
public interface TurmaRepository extends CrudRepository<Turma, Long>{

}