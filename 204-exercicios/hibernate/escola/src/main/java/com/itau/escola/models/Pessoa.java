package com.itau.escola.models;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Pessoa
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public class Pessoa {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Size(min=2)
    private String nome;

    @NotNull
    @Email
    private String email;

    @NotNull
    @OneToOne(cascade=CascadeType.ALL)
    private Endereco endereco;
    
    public long getId() {
		return id;
	}
    public void setId(long id) {
		this.id = id;
	}
    
    public String getNome() {
		return nome;
	}
    public void setNome(String nome) {
		this.nome = nome;
	}
    
    public String getEmail() {
		return email;
	}
    public void setEmail(String email) {
		this.email = email;
	}
    
    public Endereco getEndereco() {
		return endereco;
	}
    public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}