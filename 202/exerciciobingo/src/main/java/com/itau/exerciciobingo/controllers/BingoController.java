package com.itau.exerciciobingo.controllers;

import java.util.HashSet;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.itau.exerciciobingo.models.Cartela;
import com.itau.exerciciobingo.models.Sorteio;

@RestController
public class BingoController {
	
	@RequestMapping("/cartela")
	public ResponseEntity<Cartela> getCartela(@RequestParam(value="titulo", required=false) String titulo) {
		Cartela cartela;
		if(Cartela.listaDeCartelas.size()>0)
			cartela = Cartela.listaDeCartelas.get(0);
		else
			cartela = Cartela.gerarCartela(titulo);
		return ResponseEntity.ok(cartela);
	}
	
	@RequestMapping("/sorteio")
	public ResponseEntity<HashSet<Integer>> getNumeros() {
		return ResponseEntity.ok(Sorteio.novoNumero());
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/reiniciar")
	public void reiniciar() {
		Cartela.listaDeCartelas.clear();
		Sorteio.sorteados.clear();
	}

}
