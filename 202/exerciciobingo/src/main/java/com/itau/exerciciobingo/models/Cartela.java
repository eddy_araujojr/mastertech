package com.itau.exerciciobingo.models;

import java.util.ArrayList;
import java.util.HashSet;

public class Cartela {
	
	public static ArrayList<Cartela> listaDeCartelas = new ArrayList<Cartela>();
	private static int contador = 0;
	
	private int id;
	public String titulo;
	public int[][] conteudo;
	
	private Cartela() {
		this.id = contador++;
		this.titulo = "";
		this.conteudo = new int[4][4];
	}
	
	private Cartela(String titulo) {
		this.id = contador++;
		this.titulo = titulo;
		this.conteudo = new int[4][4];
	}
	
	public int getId() {
		return this.id;
	}
	
	public ArrayList<Integer> getNumerosSorteados() {
		ArrayList<Integer> sorteados = new ArrayList<Integer>();
		for (int i = 0; i < conteudo.length; i++) {
			for (int j = 0; j < conteudo[i].length; j++) {
				if(Sorteio.sorteados.contains(conteudo[i][j]))
					sorteados.add(conteudo[i][j]);
			}
		}
		return sorteados;
	}
	
	public ArrayList<Integer> getNumerosRestantes() {
		ArrayList<Integer> nSorteados = new ArrayList<Integer>();
		for (int i = 0; i < conteudo.length; i++) {
			for (int j = 0; j < conteudo[i].length; j++) {
				if(!Sorteio.sorteados.contains(conteudo[i][j]))
					nSorteados.add(conteudo[i][j]);
			}
		}
		return nSorteados;
	}

	public static Cartela gerarCartela(String titulo) {
		HashSet<Integer> sorteados = new HashSet<Integer>();
		
		int i = 0;
		while (i < 16) {
			int random = (int)(Math.ceil(Math.random() * 60));
			if(sorteados.add(random))
				i++;
		}
		
		Object[] array = sorteados.toArray();
	
		Cartela cartela = new Cartela(titulo);
		for (int j = 0; j < cartela.conteudo.length; j++) {
			for (int j2 = 0; j2 < cartela.conteudo[j].length; j2++) {
				cartela.conteudo[j][j2] = (Integer)array[i-1];
				i--;
			}
		}
		listaDeCartelas.add(cartela);
		return cartela;
	}
	
	public static Cartela gerarCartela() {
		return gerarCartela("");
	}

}
