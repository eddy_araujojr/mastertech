package com.itau.exerciciobingo.models;

import java.util.HashSet;

public class Sorteio {

	public static HashSet<Integer> sorteados = new HashSet<Integer>();

	public static HashSet<Integer> novoNumero() {
		int random;
		if (sorteados.size() < 60) {
			do {
				random = (int) (Math.ceil(Math.random() * 60));
			} while (sorteados.contains(random));
			sorteados.add(random);
		}
		return sorteados;
	}

}
