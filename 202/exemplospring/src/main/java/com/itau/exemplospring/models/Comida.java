package com.itau.exemplospring.models;

import java.util.ArrayList;


public class Comida {
	
	public static int contador = 0;
	private static ArrayList<Comida> listaDeComidas = new ArrayList<Comida>();
	
	private int id;
	public String nome;
	public String tipo;
	public int calorias;
	
	public Comida() {
	  id = contador;
	  contador++;
	  listaDeComidas.add(this);
	}
	
	public Comida(String nome, String tipo, int calorias) {
		id = contador;
		contador++;
		this.nome = nome;
		this.tipo = tipo;
		this.calorias = calorias;
		listaDeComidas.add(this);
	}
	
	public int getId()
	{
		return id;
	}
	
	public static Comida get(int id) {
		return listaDeComidas.get(id);
	}
	
	public static ArrayList<Comida> get() {
		return listaDeComidas;
	}
	
	public static boolean contains(int id) {
		return listaDeComidas.size() > id;
	}

}
