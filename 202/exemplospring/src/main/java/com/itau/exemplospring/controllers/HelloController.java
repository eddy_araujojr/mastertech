package com.itau.exemplospring.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

	@RequestMapping("/fali")
	public String olar() {
		return "Fali meu amigo!";
	}
	
	@RequestMapping("")
	public String hello() {
		return "Olá mundo!";
	}
	
	@RequestMapping(method=RequestMethod.POST, path="")
	public String helloPerson() {
		return "Olá by POST.";
	}
	
	public String novo() {
		return null;
	}
	
}
