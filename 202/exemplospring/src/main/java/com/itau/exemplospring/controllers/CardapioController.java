package com.itau.exemplospring.controllers;

import java.util.ArrayList;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
//import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.exemplospring.models.Comida;

@RestController
public class CardapioController {

	@RequestMapping("/cardapio/doces")
	public ArrayList<Comida> getDoces() {
		return Comida.get();
	}

//Forma1
//	@RequestMapping("/cardapio/doces/{id}")
//	public ResponseEntity<Comida> getDoce(@PathVariable int id) {
//		if(Comida.contains(id))
//			return ResponseEntity.status(200).body(Comida.get(id));
//		else
//			return ResponseEntity.status(404).body(null);
//	}
	
//Forma2
	@RequestMapping("/cardapio/doces/{id}")
	public ResponseEntity<?> getDoce(@PathVariable int id) {
		if(Comida.contains(id))
			return ResponseEntity.ok(Comida.get(id));
		else
			return ResponseEntity.notFound().build();
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/cardapio/doces")
	public ResponseEntity<Comida> addDoce(@RequestBody Comida comida) {
		//return comida;
		return ResponseEntity.status(201).body(comida);
	}
}
