package com.itau.saqueinternacional.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;

import com.itau.saqueinternacional.models.Customer;
import com.itau.saqueinternacional.models.WithdrawRequest;
import com.itau.saqueinternacional.repositories.CustomerRepository;
import com.itau.saqueinternacional.services.RatesConverter;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * CustomerController
 */
@RestController
public class CustomerController {

	@Autowired
	CustomerRepository customerRespository;
	RatesConverter ratesConverter;

	@RequestMapping(path="/saque")
	public Customer withdraw(@Valid @RequestBody WithdrawRequest requisicao){
		ratesConverter = new RatesConverter();
		double valor = ratesConverter.convert(requisicao.getCurrency(), requisicao.getAmount());
		return null;
	}

	@RequestMapping(path="/clientes")
	public Iterable<Customer> buscarclientes(){
		return customerRespository.findAll();
	}
}