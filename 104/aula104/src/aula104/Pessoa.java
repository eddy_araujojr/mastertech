package aula104;

public class Pessoa {
	private static int contadorIds = 0;
	
	private int id;
	float reais;
	double valorReal;
	char inicial;
	String nome;
	int[] idsRelacionados;
	
	public Pessoa() {
		nome = "";
		contadorIds++;
		id = contadorIds;
	}
	
	public Pessoa(String nome) {
		this.nome = nome;
		contadorIds++;
		id = contadorIds;
	}
	
	public int id() {
		return id;
	}
	
	public void nome(String nome) {
		this.nome = nome;
	}
	public String nome() {
		return nome;
	}
}