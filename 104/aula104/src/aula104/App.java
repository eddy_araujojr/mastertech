package aula104;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		System.out.println("Estilo cachorro!");
		System.out.println("Digite o nome a ser gravado: ");
		Scanner scanner = new Scanner(System.in);
		String nome1 = scanner.nextLine();
		double random = Math.random();
		String mensagem = "Nome: " + nome1 + "; Id: " + random;
		if (gravandoArquivo(mensagem)) {
			System.out.println("Gravado.");
			System.out.println(mensagem);
		} else {
			System.out.println("Não gravado.");
		}
		scanner.close();
	}
	
	public static void projetoteste1(String[] args) {
		System.out.println("Estilo cachorro!");
		Scanner scanner = new Scanner(System.in);
		System.out.println("Começar...");
		scanner.nextLine();
		
		System.out.println("Digite o nome do primeiro sujeito:");
		String nome1 = scanner.nextLine();
		Pessoa um = new Pessoa();
		um.nome = nome1;
		
		boolean valorAtribuido = false;
		while(!valorAtribuido) {
			System.out.println("Digite valor atribuído:");
			try {
				um.reais = scanner.nextFloat();
				valorAtribuido = true;
			} catch (java.util.InputMismatchException e) {
				System.out.println("Erro ao buscar valor.");
			} catch (Exception e) {
				um.reais = 0f;
				valorAtribuido = true;
				System.out.println("Erro desconhecido, valor 0 atribuído.");
			}
		}
	
		System.out.println("Pessoa 1 Id: " + um.id() + "; Nome: " +  um.nome);
		
		String nome2 = scanner.nextLine();
		Pessoa dois = new Pessoa(nome2);
		System.out.println("Pessoa 2 Id: " + dois.id() + "; Nome: " +  dois.nome);
		
		scanner.close();
	}
	
	public static boolean gravandoArquivo(String conteudo) {
		Path path = Paths.get("exemplo.txt");
		try {
			Files.write(path, conteudo.getBytes());
			return true;
		} catch (IOException e) {
			System.out.println("Erro na gravação de arquivo.");
		}
		return false;
	}
}
