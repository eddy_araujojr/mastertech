
const pagina = document.querySelector("body");
const links = document.querySelectorAll("a");

const inpEmail = document.querySelector("#inp_email");
const inpSenha = document.querySelector("#inp_senha");

const lblEmail = document.querySelector("#lbl_email");
const lblSenha = document.querySelector("#lbl_senha");

const divPropaganda = document.querySelector("#propaganda");

let cor_texto = "blue";

function validarEmail() {
    let email = inpEmail.value;
    let arroba = email.indexOf("@");
    let ultimoArroba = email.lastIndexOf("@");
    let ultimoPonto = email.lastIndexOf(".");

    if (email !== "" && (arroba !== ultimoArroba || arroba === -1 ||
        ultimoPonto - arroba < 3)) {
        return false;
    }
    return true;
}

function validarSenha() {
    let senha = inpSenha.value.toUpperCase();
    let numeros = /[0-9]/g;
    let letras = /[A-Z]/g;
    let retorno = false

    console.log(senha)

    let tamanho = senha.length > 5;
    let temNumero = senha.match(numeros) !== null;
    let temLetras = senha.match(letras) !== null;

    retorno = (tamanho && temNumero && temLetras) || senha === "";

    console.log(senha + " - " + retorno);
    return retorno;
}

function eventoEmail() {
    if (validarEmail()) {
        lblEmail.style.display = "none";
    }
    else {
        lblEmail.style.display = "block";
    }
}

function eventoSenha() {
    if (validarSenha()) {
        lblSenha.style.display = "none";
    }
    else {
        lblSenha.style.display = "block";
    }
}

function linksAzuis() {

    for (let link of links) {
        link.style.color = cor_texto;
    }
    if (cor_texto === "blue") {
        cor_texto = "aliceblue";
    }
    else {
        cor_texto = "blue";
    }
}

function girar() {
    this.style.transform = "rotate(180deg)";
}

function desgirar() {
    this.style.transform = "rotate(0deg)";
}

pagina.onclick = linksAzuis;

for (let link of links) {
    link.onmouseenter = girar;
    link.onmouseleave = desgirar;
}

inpEmail.onblur = eventoEmail;
inpSenha.onblur = eventoSenha;

let intervalo1 = 5000;
let intervalo2 = true;

function propaganda() {
    divPropaganda.style.display = "flex";
    
}
function propaganda2() {
    divPropaganda.style.display = "none";
    if(intervalo2) {
        setInterval(propaganda2, intervalo1);
        intervalo2 = false;
    }
}

setTimeout(propaganda2, 5100);

setInterval(propaganda, intervalo1);
