package com.itau.marketplace.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.marketplace.models.Anuncio;

public interface AnuncioRepository extends CrudRepository<Anuncio, UUID> {

}
