package com.itau.marketplace.repositories;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.itau.marketplace.models.Oferta;

public interface OfertaRepository extends CrudRepository<Oferta, UUID>{

}
