package com.itau.marketplace.models;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Oferta {
	
	@PrimaryKey
	private UUID ofertaId;
	
	@NotNull
	private String texto;
	
	private double preco;
	
	private UUID anuncioId;
	
	private UUID userId;
	
	private String username;
	
	private String nome;
	
	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean isAprovado() {
		return isAprovado;
	}

	public void setAprovado(boolean isAprovado) {
		this.isAprovado = isAprovado;
	}

	private boolean isAprovado;

	public UUID getOfertaId() {
		return ofertaId;
	}

	public void setOfertaId(UUID ofertaId) {
		this.ofertaId = ofertaId;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public UUID getAnuncioId() {
		return anuncioId;
	}

	public void setAnuncioId(UUID anuncioId) {
		this.anuncioId = anuncioId;
	}
	
	

}
