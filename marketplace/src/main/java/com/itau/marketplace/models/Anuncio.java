package com.itau.marketplace.models;

import java.time.LocalDate;
import java.util.UUID;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

@Table
public class Anuncio {

	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED)
	private UUID anuncioId;
	
	@NotNull
	private String titulo;
	
	@NotNull
	private String descricao;
	
	private double preco;
	
	private LocalDate data;
	
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private UUID userId;
	
	@NotNull
	private String nomeUsuario;
	
	@NotNull
	private String username;
	
	@Email
	private String email;

	public UUID getAnuncioId() {
		return anuncioId;
	}

	public void setAnuncioId(UUID anuncioId) {
		this.anuncioId = anuncioId;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public LocalDate getData() {
		return data;
	}

	public void setData(LocalDate data) {
		this.data = data;
	}

	public UUID getUserId() {
		return userId;
	}

	public void setUserId(UUID userId) {
		this.userId = userId;
	}

	
	public String getNomeUsuario() {
		return nomeUsuario;
	}

	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
