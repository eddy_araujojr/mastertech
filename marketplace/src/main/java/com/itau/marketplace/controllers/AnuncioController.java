package com.itau.marketplace.controllers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.marketplace.models.Anuncio;
import com.itau.marketplace.repositories.AnuncioRepository;

@RestController
@RequestMapping("/anuncios")
public class AnuncioController {
	
	@Autowired
	AnuncioRepository usuarioRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscarAnuncios() {
		return usuarioRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Anuncio criarAnuncio(@RequestBody Anuncio anuncio) {
		anuncio.setAnuncioId(UUID.randomUUID());
		return usuarioRepository.save(anuncio);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public Anuncio atualizarAnuncio(@RequestBody Anuncio anuncio) {
		return usuarioRepository.save(anuncio);
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public Anuncio removerAnuncio(@RequestBody Anuncio anuncio) {
		usuarioRepository.delete(anuncio);
		return anuncio;
	}

}
