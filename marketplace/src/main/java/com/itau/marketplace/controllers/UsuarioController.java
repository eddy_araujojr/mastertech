package com.itau.marketplace.controllers;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.marketplace.models.Usuario;
import com.itau.marketplace.repositories.UsuarioRepository;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {
	
	@Autowired
	UsuarioRepository usuarioRepository;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<?> buscarUsuarios() {
		return usuarioRepository.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Usuario criarUsuario(@RequestBody Usuario usuario) {
		usuario.setUserId(UUID.randomUUID());
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public Usuario atualizarUsuario(@RequestBody Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	
	@RequestMapping(method=RequestMethod.DELETE)
	public Usuario removerUsuario(@RequestBody Usuario usuario) {
		usuarioRepository.delete(usuario);
		return usuario;
	}

}